import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  // Registration Details
  regDetails = {
      "profile_id": "",
      "email": "",
      "first_name": "",
      "surname ": "",
      "id_type": "",
      "id_number": ""
  };
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

}
