import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../home/authenticate';

@Component({
  selector: 'page-details',
  templateUrl: 'details.html'
})
export class DetailsPage {

  cust_details = [];

  constructor( public navCtrl: NavController, public navParams: NavParams, 
               public authService : AuthService ) {}

  ionViewDidLoad() {
     this.getCustomerDetails();
  }

  /**
   * Retrieve Customer details
   */
  getCustomerDetails() {
    Promise.resolve( this.authService.getCustomerDetails() ).then( result => {
      //Extract & asssign the nested key-pairs to the main Array
      result["customer_Identification_Id"]=result.identification_List[0].customer_Identification_Id;
      result["iD_Number"]=result.identification_List[0].iD_Number;
      result["identification_Type_Id"]=result.identification_List[0].identification_Type_Id;
      
      this.cust_details = result;  
    });
  }  

}
