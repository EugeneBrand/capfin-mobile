import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { AuthService } from './authenticate';
import { UserpagePage } from '../userpage/userpage';
import { SignupPage } from '../signup/signup';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  usercreds = {
    // Temporarily hardcoded values for testing purposes => empty when done
    email : 'example1131@capfin.co.za',
    password : 'password123'
  };

  loading : any;

  constructor(private navCtrl: NavController, private authService : AuthService,
              private loadingContr : LoadingController, private alertCtrl: AlertController ) {}
  /*
   Log in
  */
  login( user ) {
    // Present loading indicator
    this.showLoading();   
    // Authenticate client details
    Promise.resolve( this.authService.authClient( user ) )
           .then( result => {

            if( result ) {
            setTimeout(() => {
              this.loading.dismiss();
              this.navCtrl.setRoot( UserpagePage );      
            });        
          } else {
            this.showError( "Access Denied" );
          }
    }, reject => {
      this.showError( reject );
    });
  }
  /*
    Sends user to account creation page
   */
  createAccount() {
    this.navCtrl.push( SignupPage );
  }
  /*
    Loading spinner 
  */
  showLoading() {
    this.loading = this.loadingContr.create({
      spinner : 'crescent',
      showBackdrop : false
  });    
    this.loading.present();
  }
  /* 
    Display errors
  */
  showError( text ) {
    setTimeout(() => {
      this.loading.dismiss();
    });

    let alert = this.alertCtrl.create({
      title : "Fail",
      subTitle : text,
      buttons: ["OK"]
    });
    alert.present( prompt );
  }

}
