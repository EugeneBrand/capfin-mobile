import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../home/authenticate';

@Component({
  selector: 'page-accounts',
  templateUrl: 'accounts.html'
})
export class AccountsPage {

  accounts = [];
  _thisAccount = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public authService : AuthService) {}

  ionViewDidLoad() {
    this.getAccounts();
  }

  getAccounts() {
    this.authService.getAccountsList().then( result => {
      this.accounts = result;    
    });
  }

  getAccountDetails( account_No ) {
    this.authService.getSpecificAccount( account_No ).then( result => {
      this._thisAccount = result;
    });
  }

}
